package com.example.SpringPSS.repositories;

import com.example.SpringPSS.entities.Report;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportRepository  extends CrudRepository<Report, Long> {
    Report findRaportById(int id);
    List<Report> findByUserId(int id);
}
