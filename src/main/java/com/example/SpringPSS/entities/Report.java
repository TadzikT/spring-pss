package com.example.SpringPSS.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Report {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private int privePerDay;

    @Column(nullable = false, length = 20)
    private String below900cc;

    @Column(nullable = false, length = 20)
    private String above900cc;

    @Column(nullable = false, length = 40)
    private String motocycle;

    @Column(nullable = false, length = 40)
    private String motorbike;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id", nullable = false)
    @JsonBackReference
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public int getPrivePerDay() {
        return privePerDay;
    }

    public void setPrivePerDay(int privePerDay) {
        this.privePerDay = privePerDay;
    }

    public String getBelow900cc() {
        return below900cc;
    }

    public void setBelow900cc(String below900cc) {
        this.below900cc = below900cc;
    }

    public String getAbove900cc() {
        return above900cc;
    }

    public void setAbove900cc(String above900cc) {
        this.above900cc = above900cc;
    }

    public String getMotocycle() {
        return motocycle;
    }

    public void setMotocycle(String motocycle) {
        this.motocycle = motocycle;
    }

    public String getMotorbike() {
        return motorbike;
    }

    public void setMotorbike(String motorbike) {
        this.motorbike = motorbike;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
